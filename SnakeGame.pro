#-------------------------------------------------
#
# Project created by QtCreator 2013-11-07T16:23:13
#
#-------------------------------------------------

QT       += core gui

TARGET = SnakeGame
TEMPLATE = app


SOURCES += \
    Snake.cpp

HEADERS  += \
    Snake.h
