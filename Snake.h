#ifndef SNAKE_H
#define SNAKE_H
#include <QDialog>
#include <QTimer>
#include <QList>
#include <QLabel>
#include <QKeyEvent>
#include <QTimerEvent>
enum  Direction {d_up,d_down,d_left,d_right};
class  Snake:public QDialog{
    Q_OBJECT
    private:
    QLabel* food;
    QTimer* timer;
    QList<QLabel*> data;
    Direction d;
    int maxLen;
    int speed;
public:
    Snake();
    ~Snake();
    QLabel * getFood();
public  slots:
    void move();
protected :
    void keyPressEvent(QKeyEvent *);
    void timerEvent(QTimerEvent *);
};
#endif // SNAKE_H
