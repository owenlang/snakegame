#include "Snake.h"
#include <QFrame>
#include <QPalette>
#include <QColor>
#include <QApplication>
#include <QMessageBox>
#include <QTime>
#include <time.h>
Snake::Snake(){
    qsrand(QTime().currentTime().msec());
   this->resize(400,400);
   speed=20;
   d=d_right;
   data.push_back(getFood());
   /*data[0]->resize(20,20);
   data[0]->move(10*20,10*20);
   data[0]->setAutoFillBackground(true);
   data[0]->setFrameShape(QFrame::Box);
   data[0]->setPalette(QPalette(
           QColor(qrand()%255,
                  qrand()%110,qrand()%255)));
   data[0]->show();
   */
   timer=new QTimer();
   timer->setInterval(400);
   timer->start();
   //this->startTimer(300);
   connect(timer,SIGNAL(timeout()),
           this,SLOT(move()));
   //生成食物  调用方法
   food=getFood();
   food->show();
   maxLen=5;
  // this->startTimer(400);

}
Snake::~Snake(){
    while(data.size()>0){
    delete  data[0];
    data.removeAt(0);;
    }
}
QLabel * Snake::getFood(){
int w=this->width();
int h=this->height();
qDebug("%d:%d",w/20,h/20);
//随机 生成 一个数

 int mx= (qrand()%(w/20))*20;
 int my= (qrand()%(h/20))*20;
 food=new QLabel(this);
 food->move(mx,my);
 food->resize(20,20);
 food->setAutoFillBackground(true);
 food->setFrameShape(QFrame::Box);
 food->setFrameShadow(QFrame::Sunken);
 food->setPalette(QPalette(
         QColor(qrand()%255,
                qrand()%110,qrand()%255)));
 return food;
}
void Snake::move(){
    //蛇头的下一个位置
    int nheadx=data[0]->x();
    int nheady=data[0]->y();
    //得到食物坐标
    int foodx=food->x();
    int foody=food->y();
    if(nheadx==foodx&&nheady==foody){
      data.push_back(food);
      food=getFood();
      food->show();
      qDebug("test");
    }
    //game over

    if(data.size()>=5){
     QMessageBox msg(this);
     msg.setText("game over");
     msg.setStandardButtons(QMessageBox::Yes|QMessageBox::No);
     msg.show();
     if(msg.exec()==QMessageBox::Yes){
      this->close();
     }


    }
    switch(d){
        case d_up:
        nheady-=speed;
        break;
        case d_down:
        nheady+=speed;
        break;
        case d_left:
        nheadx-=speed;
        break;
        case d_right:
        nheadx+=speed;
        break;
    }
    //后面的元素跟着前面的 移动
    for(int i=data.size()-1;i>0;i--){
    data[i]->move(data[i-1]->x(),
                  data[i-1]->y());
    }
    data[0]->move(nheadx,nheady);
}
void Snake::keyPressEvent(QKeyEvent *e){
    if(e->key()==Qt::Key_Up){
      d=d_up;
    }else if(e->key()==Qt::Key_Down){
        d=d_down;
    }else if(e->key()==Qt::Key_Left){
        d=d_left;
    }else if(e->key()==Qt::Key_Right){
        d=d_right;
    }
}
void Snake::timerEvent(QTimerEvent *){
    move();
}
int main(int argc,char **argv){
QApplication app(argc,argv);
Snake s;
s.show();
return app.exec();
}
